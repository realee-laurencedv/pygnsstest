# unit test with pytest

import pytest
import os
import pathlib
import logging
from pygnsstest.pygnsstest import PyGNSSTest as pgt
from pygnsstest.version import *


def abs_path_4_test(path):
    curdir = os.getcwd()
    return os.path.normpath(os.path.join(curdir, path))


# ============ ARG - version ============ #


def test_arg_version(capsys):
    with pytest.raises(SystemExit):
        pgt._parse_args(pgt, args=['--version'])

    captured = capsys.readouterr()  # capture stdout and stderr
    assert f'pygnsstest.pygnsstest {version}' in captured.out

# ============ ARG - help ============ #


def test_arg_help(capsys):
    with pytest.raises(SystemExit):
        pgt._parse_args(pgt, args=['--help'])

    captured = capsys.readouterr()  # capture stdout and stderr
    assert f'(default: ' in captured.out  # ensure defaults are printed


# ============ ARG - invalid ============ #

@pytest.mark.parametrize(('test_arg', 'expected'), [
    (['--patate'], 'unrecognized arguments: --patate'),
    (['--pa', '0554'], 'unrecognized arguments: --pa'),
]
)
def test_arg_invalid(test_arg, expected, capsys):
    with pytest.raises(SystemExit):
        pgt._parse_args(pgt, args=test_arg)

    captured = capsys.readouterr()  # capture stdout and stderr
    captured = captured.out + captured.err
    assert expected in captured


# ============ ARG - log ============ #


@pytest.mark.parametrize(
    ('test_arg', 'expected'), [
        (['--log', 'log'], pathlib.Path('log')),
        (['--log', 'log/subdir'], pathlib.Path('log/subdir')),
        (['--log', 'log/subdir/.'], pathlib.Path('log/subdir')),
        (['--log', '/'], pathlib.Path('/')),
        (['--log', '/home/user/patate'], pathlib.Path('/home/user/patate')),
        (['--log', '8956'], pathlib.Path('8956')),
        (['--log', '/dir/../patate'], pathlib.Path('/dir/../patate')),
    ]
)
def test_arg_log(test_arg, expected):
    actual = pgt._parse_args(pgt, args=test_arg)
    assert actual.__contains__('log')
    assert actual.log == expected

# ============ ARG - noconsole ============ #


@pytest.mark.parametrize(
    ('test_arg', 'expected'), [
        ('', False),
        (['--noconsole'], True)
    ]
)
def test_arg_noconsole(test_arg, expected):
    actual = pgt._parse_args(pgt, args=test_arg)
    assert actual.__contains__('noconsole')
    assert actual.noconsole == expected

# ============ ARG - verbose ============ #


@pytest.mark.parametrize(
    ('test_arg', 'expected'), [
        ('', logging.CRITICAL),
        (['-v'], logging.ERROR),
        (['-vv'], logging.WARNING),
        (['-vvv'], logging.INFO),
        (['-vvvv'], logging.DEBUG),
        (['-vvvvv'], logging.DEBUG),
        (['--verbose'], logging.ERROR),
        (['--verbose', '--verbose'], logging.WARNING),
        (['--verbose', '-v'], logging.WARNING),
        (['--verbose', '-vv'], logging.INFO),
        (['--verbose', '-v', '-v'], logging.INFO),
        (['-v', '-v', '-v', '-v'], logging.DEBUG),
        (['-v', '-v', '-v', '-v', '-v'], logging.DEBUG),
    ]
)
def test_arg_verbose(test_arg, expected):
    actual = pgt._parse_args(pgt, args=test_arg)
    assert actual.__contains__('verbose')
    assert actual.verbose == expected
