# pygnsstest

[pygnsstest][home_link] is a utility for testing performance and characterizing gnss receivers.  
It can compare concurrently stream from serial ports or work with log files.  
It only supports [NMEA][nmea_link] protocol as of now and make heavy usage of the [pynmeagps][pynmeagps_link] python lib

## Install

### Stable version

_Stable_ package are release on [pypi.org][pypi_link] and are installable simply via pip:

```bash
pip install pygnsstest
```

You can also clone the specific tags from the [repo][repo_link]

### Development version

_Development version_ are only available on the [repo][repo_link], the suggested process is to add a **git submodule** to your project:

1. Add the submodule  
   simply open a **terminal** in the host repo and execute this:

   ```bash
   git submodule add https://gitlab.com/realee-laurencedv/pygnsstest.git dep/pygnsstest
   ```

2. Venv install
   Then you need to [install](https://laurencedv.org/computing/python) the venv, by having [poetry][poetry_link] and [pyenv][pyenv_link].  
   Open a **terminal** then execute this:

   ```bash
   poetry install
   ```

## Usage

## Contrib

See the [contribution guideline][contrib_file]

## Changelog

See the [release][release_file] file and [roadmap file][roadmap_file]

## License

This software is released under [GPL3][license_file]

<!-- project links -->

[home_link]: https://gitlab.com/realee-laurencedv/pygnsstest
[repo_link]: https://gitlab.com/realee-laurencedv/pygnsstest
[pypi_link]: https://pypi.org/project/pygnsstest/

<!-- links -->

[poetry_link]: https://python-poetry.org/docs/
[pyenv_link]: https://github.com/pyenv/pyenv
[nmea_link]: https://en.wikipedia.org/wiki/NMEA_0183
[pynmeagps_link]: https://pypi.org/project/pynmeagps/

<!-- files -->

[release_file]: doc/release.md
[roadmap_file]: doc/roadmap.md
[contrib_file]: doc/contrib.md
[license_file]: license
