import sys

from .pygnsstest import PyGNSSTest


def main():
    obj = PyGNSSTest(sys.argv[1:])
    obj.run()


if __name__ == "__main__":
    main()
