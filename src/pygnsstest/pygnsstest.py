from . import version
import os
import time
from datetime import datetime
from time import sleep
import argparse
import pathlib
from rich import print, box
from rich.live import Live
from rich.align import Align
from rich.console import Console, Group
from rich.layout import Layout
from rich.panel import Panel
from rich.progress import BarColumn, Progress, SpinnerColumn, TextColumn
from rich.syntax import Syntax
from rich.table import Table

# logging
from multiprocessing import Process, Queue, current_process
from logging.handlers import TimedRotatingFileHandler, QueueHandler
from rich.logging import RichHandler
import logging


class PyGNSSTest(object):

    __fps__ = 10
    __verb_levels__ = [logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]

    def __init__(self, args):
        super(PyGNSSTest, self).__init__()
        self.time_start = time.time()

        # Arguments
        self.args = self._parse_args(args)  # save all parsed arguments

        # region Logger
        self.log_q = Queue()

        # main object logger
        self.logger = logging.getLogger('pygnsstest')
        self.logger.setLevel(self.args.verbose)
        self.logger.addHandler(QueueHandler(self.log_q))

        logger_p = Process(target=self.logger_process, args=(self.log_q,))
        logger_p.start()
        self.logger.debug(f'log queue: {id(self.log_q)}')
        # endregion

        # log args
        # self.logger.debug(f'args - input path: {self.args.input}')

        # # region layout
        self.layout = self._make_layout(4)
        # endregion

        self.time_creation = time.time()
        self.logger.debug(f'{__name__} created successfully in {self.time_creation - self.time_start:.6f} sec')

    def _parse_args(self, args=None):

        # Parse them from console input
        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, prog=__name__)
        parser.add_argument("-v", "--verbose", help="increase the verbosity level (can be used upto 4 times)", action='count', default=0)
        parser.add_argument("--log", help="file logging directory", type=pathlib.Path)
        parser.add_argument("--noconsole", help="disable console logging", default=False, action="store_true")
        parser.add_argument("--version", action="version", version=f'%(prog)s {version.version}')
        result = parser.parse_args(args)  # Parse all arguments

        # Sanitize everything
        # verbose
        result.verbose = self.__verb_levels__[min(result.verbose, len(self.__verb_levels__) - 1)]

        return result

    def run(self):
        status = 0
        execution = True
        self.logger.info(f'S execution, pid: {current_process().pid}')

        with Live(self.layout, refresh_per_second=self.__fps__, screen=True):
            while execution:
                sleep(0.1)
                pass

        self.time_done = time.time()

        # graceful end of exec
        self.logger.info(f'P execution, pid: {current_process().pid}, total elapse {self.time_done - self.time_start:.6f} sec')
        self.log_q.put(None)
        return status

    # =========================== Processes =========================== #

    def logger_process(self, queue: Queue) -> int:
        """ logging isolated process

        Args:
            queue (Queue): logging queue receiving all logs to be saved
        """

        logger = logging.getLogger('logger')
        logger.setLevel(self.args.verbose)

        # Create a separate folder for logs if it doesn't exist
        if self.args.log is not None and not os.path.exists(self.args.log):
            os.makedirs(self.args.log)

        # Formater
        formatter = logging.Formatter('%(asctime)s.%(msecs)03d | %(name)-15s | %(levelname)-8s | %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')
        rich_formatter = logging.Formatter('[light_sea_green]%(name)-15s[/] %(message)s')

        # File logging
        if self.args.log is not None:
            log_filename = os.path.join(self.args.log, f'pygnsstest_{time.strftime("%Y-%m-%d_%H-%M-%S")}.log')
            file_handler = TimedRotatingFileHandler(log_filename, when='midnight', backupCount=4)
            file_handler.setFormatter(formatter)

        # Console logging
        if not self.args.noconsole:
            rich_handler = RichHandler(rich_tracebacks=True, markup=True)
            rich_handler.setLevel(self.args.verbose)
            rich_handler.setFormatter(rich_formatter)

        if self.args.log is not None:
            logger.addHandler(file_handler)
        if not self.args.noconsole:
            logger.addHandler(rich_handler)
        else:
            logger.debug(f'no console logging requested')

        logger.info(f'S execution, pid: {current_process().pid}')
        if self.args.log is not None:
            logger.debug(f'file logging in {self.args.log}')

        # process loop
        while True:
            msg = queue.get()       # pull new message from queue
            if msg is None:         # exit process on special None message
                break
            logger.handle(msg)      # log the message

        logger.info(f'P execution, pid: {current_process().pid}')
        return 0

    # =========================== UI =========================== #

    class Header:
        """Display header with real-time stats"""

        def __rich__(self) -> Panel:
            # stat grid
            grid_stat = Table.grid(expand=False)
            grid_stat.add_column(justify="right", width=20)
            grid_stat.add_column(justify="left", width=30)
            grid_stat.add_row(
                "[b]cpu time[/b]: ", datetime.now().ctime().replace(":", "[blink]:[/]"),
            )

            # top level grid
            grid_top = Table.grid(expand=True)
            grid_top.add_column(justify="left", width=50)
            grid_top.add_column()
            grid_top.add_row(grid_stat, None)

            return Panel(grid_top, style="green on grey23", title=f'PyGNSStest {version.version}')

    class Device:
        """Display device with real-time console"""

        def __init__(self, dev_id: int) -> None:
            self.dev_id = dev_id

        def __rich__(self) -> Panel:
            # param grid
            grid_param = Table.grid(expand=False)
            grid_param.add_column(justify="left", width=15)
            grid_param.add_column(justify="left", width=50)
            grid_param.add_row('path: ', '----')

            # top level grid
            grid_top = Table.grid(expand=True)
            grid_top.add_column(justify="center")
            grid_top.add_row(grid_param)

            return Panel(grid_top, style="pale_green1 on grey19", title=f'Device {self.dev_id}')

    def _make_layout(self, deviceNb=1) -> Layout:
        """ Create layout structure """
        layout = Layout(name="root")

        layout.split(
            Layout(name="header", ratio=1),
            Layout(name="devices", ratio=2)
        )
        layout["header"].update(self.Header())

        for i in range(0, deviceNb):
            layout["devices"].split_row(
                Layout(name=f'device{i}', minimum_size=80, size=120),
            )
            layout[f'device{i}'].update(self.Device(i))

        return layout
