# pygnsstest releases

See what is planned in the [roadmap][roadmap_file]

## 0.1.0

Release date: _2024-04-??_

**Feature:**

- Initial functional release
- development venv setup
- logging in multiprocessing thread
- basic functional test for diagram and harness
- selectable verbosity in console and log (one argument for both)
- buildable & deployable with poetry

**Known problem:**

- placeholder

---

[roadmap_file]: roadmap.md
