# HWDOCER Roadmap

## 0.1.0

- [x] venv setup
- [ ] python executable packaging (deployed on pypi)
- [x] multiprocessing logging
- [ ] basic functional test
- [x] selectable verbosity in console and log (one argument for both)
- [ ] cli interface structured

## 0.2.0

- [ ] ground-truth source designation
- [ ] individual satellite snr comparison in interface
- [ ] real-time point by point to ground-truth with multiple source
- [ ] agglomerated and separated measurement file logging

## 0.3.0

- [ ] gnss receiver identification
- [ ] unit testing for all major functionality

## 0.4.0

- [ ] antenna fov characterization

## 0.5.0

- [ ] fixed position averaging

## 0.6.0

- [ ] Functional testing of all major functionality

## 1.0.0

- [ ] Stable execution
