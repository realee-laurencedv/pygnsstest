# Contrib

## Developer setup

### VSCode

1. Install [VSCode][vscode_link] following the [guide][vscode_install_link]
2. Open the repo root folder

   ```bash
   code /path/to/folder
   ```

3. Install venv with dev dependencies

   ```bash
   poetry install --with dev
   ```

   > Also doable from [command palette](https://laurencedv.org/computing/vscode#command-palette) with the [poetry vscode plugin](https://laurencedv.org/computing/vscode-plugins#python)

4. select python interpreter:  
   `CTRL` + `SHIFT` + `P` , then type _python: select interpreter_ and select the newly installed venv.
   > If you don't see it, you can use the _enter path_ option and use `poetry env info --path` in the terminal to find the path

## Running tests

1. Open the `Run and Debug` tab in VSCode
2. Select the test you want to run and press `Start Debugging` or `F5`

## Release process

1. _Run_ the [tests][test_folder] and verify everything is good to go
2. _Ensure_ the [roadmap file][roadmap_file] is updated
3. _Write_ the features, fixes, removals and knowns problem to [release file][release_file]
4. _Invoke_ bumpver: `bumpver update --major [--dry]`
5. _Build_ the wheel: `poetry build`
6. _Publish_ the new version: `poetry publish`

<!-- links -->

[vscode_link]: https://code.visualstudio.com/
[vscode_install_link]: https://laurencedv.org/computing/vscode

<!-- files -->

[release_file]: doc/release.md
[roadmap_file]: doc/roadmap.md

<!-- folders -->

[test_folder]: test/
